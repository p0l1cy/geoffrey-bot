import asyncio
from typing import List

import httpx
from bitcoinaddress import Wallet
from bs4 import Tag

import html_text_utils
from utils import async_run


@async_run
async def get_top_wallets() -> str:
    async with httpx.AsyncClient() as client:
        response = await client.get(
            "https://bitinfocharts.com/top-100-richest-bitcoin-addresses.html"
        )
        top_wallets = parse_top_wallets(response.text)
        top_wallets_list = "\n\n".join(top_wallets)
        return f"TOP LOTTERY PRICES:\n{top_wallets_list}"


def parse_top_wallets(html: str) -> List[str]:
    result_addresses: List[str] = []
    html_address: Tag
    bs = html_text_utils.to_bs(html)
    address_table = bs.find("table", {"id": "tblOne"})
    html_address_list = address_table.findAll("tr")
    for html_address in html_address_list[1:]:
        columns = html_address.findAll("td")
        number = columns[0].text
        address = short_string(columns[1].text.split("wallet")[0])
        address_balance = columns[2].text.split(")")[0]
        address_result = f"{number} - {address}\nPrice: {address_balance})"
        result_addresses.append(address_result)
    if len(result_addresses) > 5:
        return result_addresses[:5]
    return result_addresses


def short_string(string: str):
    if len(string) > 12:
        return f"{string[:7]}...{string[-3:]}"
    return string


async def get_btc_balance(address: str) -> dict:
    response: httpx.Response
    client: httpx.AsyncClient
    async with httpx.AsyncClient() as client:
        response = await client.get(
            f"https://www.cointracker.io/wallet/bitcoin?address={address}"
        )
        csrf_token = _parse_crf_token(response.text)
        post_data = _create_post_data(csrf_token, address)
        post_headers = _get_post_headers(address)
        response = await client.post(
            "https://www.cointracker.io/wallet_balance/poll",
            content=post_data,
            headers=post_headers,
        )
        if "false" in response.text:
            await asyncio.sleep(5)
            return await get_btc_balance(address)
        return response.json()


def _get_post_headers(address: str) -> dict:
    referer = f"https://www.cointracker.io/wallet/bitcoin?address={address}"
    return {
        "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
        "x-requested-with": "XMLHttpRequest",
        "referer": referer,
    }


def _create_post_data(csrf_token: str, address: str) -> str:
    return f"csrf_token={csrf_token}&address={address}"


def _parse_crf_token(html: str) -> str:
    bs = html_text_utils.to_bs(html)
    csrf_token = bs.find("input", {"id": "csrf_token"}).get("value")
    return csrf_token


@async_run
async def generate_btc_wallet() -> str:
    wallet = Wallet()
    # seed = wallet.key.seed
    # wif = wallet.key.mainnet.wif
    # wifc = wallet.key.mainnet.wifc
    priv_key = wallet.key.hex
    pubaddr1 = wallet.address.mainnet.pubaddr1
    # pubaddr1c = wallet.address.mainnet.pubaddr1c
    # pubaddr3 = wallet.address.mainnet.pubaddr3
    # pubaddrbc1_P2WPKH = wallet.address.mainnet.pubaddrbc1_P2WPKH
    # pubaddrbc1_P2WSH = wallet.address.mainnet.pubaddrbc1_P2WSH
    balance = await get_btc_balance(pubaddr1)
    balance_btc = balance["balance"]
    balance_usd = balance["usd_value"]
    if int(balance_btc) > 0:
        result_message = "AMAZING!! YOU WON o.O"
    else:
        result_message = "More luck next time..."
    result = (
        f"Created btc address:\n\n"
        f"Pub_key: {pubaddr1}\n"
        f"Priv_key: {priv_key}\n"
        f"Price: {balance_btc}, {balance_usd}\n\n"
        f" {result_message}"
    )
    return result
