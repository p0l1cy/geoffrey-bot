import asyncio
from functools import wraps
from typing import Callable


def async_run(f: Callable) -> Callable:
    """A wrapper that makes it easy to run an async function.
    Convenient for reducing boilerplate in examples.
    """

    @wraps(f)
    def wrapper(*args, **kwargs):
        return asyncio.run(f(*args, **kwargs))

    return wrapper
